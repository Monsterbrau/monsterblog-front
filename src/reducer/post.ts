import * as actions from '../action/post';
import { PostModel } from '../model/postModel';
import { ActionType } from 'typesafe-actions';

type PostActions = ActionType<typeof actions>;

export interface PostState {
  posts: PostModel[];
}

const initialState: PostState = {
  posts: [],
};

export default (state = initialState, action: PostActions) => {
  switch (action.type) {
    case 'RECEIVE_CHANGE_POST':
      return {
        ...state,
        contacts: state.posts.map(c =>
          c.id === action.payload.id ? action.payload : c
        ),
      };
    case 'RECEIVE_POSTS':
      return {
        ...state,
        contacts: action.payload,
      };
    case 'RECEIVE_ADD_POST':
      return {
        ...state,
        contacts: state.posts.concat(action.payload),
      };
    case 'RECEIVE_DELETE_POST':
      return {
        ...state,
        contacts: state.posts.filter(c => c.id !== action.payload),
      };
    default:
      return state;
  }
};
