import  {action}  from 'typesafe-actions';
import { PostModel, PostModelWithoutId } from '../model/postModel';


export const addPost = (post: PostModelWithoutId) => action('ADD_POST', post);
export const updatePost = (post: PostModel) => action('UPDATE_POST', post);
export const deletePost = (id: number) => action('DELETE_POST', id);

export const receivePosts = (posts: PostModel[]) => action('RECEIVE_POSTS', posts);
export const receiveChangePost = (post: PostModel) => action('RECEIVE_CHANGE_POST', post);
export const receiveAddPost = (post: PostModel) => action('RECEIVE_ADD_POST', post);
export const receiveDeletePost = (id: number) => action('RECEIVE_DELETE_POST', id);
