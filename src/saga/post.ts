import * as api from '../api';
import { put, takeEvery, all } from 'redux-saga/effects';
import {
  addPost,
  deletePost,
  receiveAddPost,
  receiveChangePost,
  receiveDeletePost,
  receivePosts,
  updatePost,
} from '../action/post';

export function* fetchPostSaga() {
  const posts = yield api.fetchPosts();
  yield put(receivePosts(posts));
}

function* addPostSaga(action: ReturnType<typeof addPost>) {
  const post = yield api.addPost(action.payload);
  yield put(receiveAddPost(post));
}

function* deletePostSaga(action: ReturnType<typeof deletePost>) {
  yield api.deletePost(action.payload);
  yield put(receiveDeletePost(action.payload));
}

function* updatePostSaga(action: ReturnType<typeof updatePost>) {
  yield api.updatePost(action.payload);
  yield put(receiveChangePost(action.payload));
}

/******* WATCHERS *********/
function* watchPostSaga() {
  yield takeEvery('LOAD_POST', fetchPostSaga);
}

function* watchUpdateSaga() {
  yield takeEvery('UPDATE_POST', updatePostSaga);
}

function* watchAddPostSaga() {
  yield takeEvery('ADD_POST', addPostSaga);
}

function* watchDeletePostSaga() {
  yield takeEvery('DELETE_POST', deletePostSaga);
}

export default function* contactSaga() {
  yield fetchPostSaga();
  yield all([
    watchPostSaga(),
    watchUpdateSaga(),
    watchAddPostSaga(),
    watchDeletePostSaga()
  ]);
}
