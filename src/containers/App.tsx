import React from "react";
import {Admin, Resource } from 'react-admin';
import { Provider } from "react-redux";
import { getStore } from "../store";
import "./App.css";
import { PostCreate } from "../component/PostCreate";
import { PostUpdate } from "../component/PostUpdate";
import { PostList } from "../component/PostList";


//const dataProvider = jsonServerProvider('http://jsonplaceholder.typicode.com');

const App = () => (
  <Admin dataProvider={Provider} store={getStore}>
    <Resource name='posts' list={PostList} create={PostCreate} edit={PostUpdate}/>
  </Admin>
);

export default App;
