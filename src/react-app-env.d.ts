/// <reference types="react-scripts" />
declare module 'react-admin';
declare module 'ra-data-json-server';
declare module 'ra-i18n-polyglot';
declare module 'ra-language-french';
declare module 'ra-language-english';