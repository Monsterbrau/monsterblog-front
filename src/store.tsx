import { createStore, applyMiddleware } from 'redux';
import reducer from './reducer/post';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './saga/index';

const sagaMiddleware = createSagaMiddleware();

export const getStore = () => {
  const store = createStore(
    reducer,
    applyMiddleware(sagaMiddleware)
  );
  sagaMiddleware.run(rootSaga);
  return store;
};