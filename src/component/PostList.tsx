import React from "react";
import { List, Datagrid, TextField } from "react-admin";

export const PostList = (props: any) => (
  <List {...props}>
    <Datagrid>
      <TextField source="id" />
      <TextField source="subtitle" />
      <TextField source="title" />
      <TextField source="text" />
    </Datagrid>
  </List>
);