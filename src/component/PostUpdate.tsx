import React from 'react';
import {Edit, SimpleForm, TextInput} from 'react-admin';

export const PostUpdate = (props:any) => (
    <Edit {...props}>
        <SimpleForm>
            <TextInput disabled source='id'/>
            <TextInput source='title' label='Title'/>
            <TextInput source='subtitle' label='Subtitle'/>
            <TextInput multiline source='text' label='Text'/>
        </SimpleForm>
    </Edit>
);