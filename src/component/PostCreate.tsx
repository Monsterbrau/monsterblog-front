import React from 'react';
import {Create, SimpleForm, TextInput} from 'react-admin';

export const PostCreate = (props:any) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput  source='id' label='ID'/>
            <TextInput source='title' label='Title'/>
            <TextInput source='subtitle' label='Subtitle'/>
            <TextInput multiline source='text' label='Text'/>
        </SimpleForm>
    </Create>
);