import { PostModel, PostModelWithoutId } from './model/postModel';

const API_URL = 'http://localhost:3004/posts';

export const fetchPosts = async () => {
  const result = await fetch(API_URL);
  const posts: PostModel[] = await result.json();
  return posts;
};

export const updatePost = async (updatePost: PostModel) => {
  const result = await fetch(`${API_URL}/${updatePost.id}`, {
    method: 'PATCH',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({updatePost}),
  });
  const post: PostModel = await result.json();
  return post;
};

export const addPost = async (postToAdd: PostModelWithoutId) => {
  const result = await fetch(API_URL, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(postToAdd),
  });
  const post: PostModel = await result.json();
  return post;
};

export const deletePost = async (id: number) => {
  await fetch(`${API_URL}/${id}`, {
    method: 'DELETE',
  });
};
