type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

export interface PostModel {
  id: number;

  title: string;

  subtitle: string;

  text: string
}

export type PostModelWithoutId = Omit<PostModel, 'id'>;